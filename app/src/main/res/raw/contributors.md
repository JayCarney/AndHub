<!--
This file contains references to people who contributed to the app.
You can also send a mail to [WIP](http://WIP) to get included.

Schema:  **[Name](Reference)**<br/>~° Text

Where:
  * Name: username, first/lastname
  * Reference: E-Mail, Webpage
  * Text: Information about / kind of contribution



## LIST OF CONTRIBUTORS
-->
**[Dandelion*](https://github.com/gsantner/dandelion)**<br/>~° Starting fork for Nomad <br/>
**[Massimiliano](https://hub.disroot.org/channel/massimiliano)**<br/>~° Current developer of Nomad<br/>
**[gia vec](https://framagit.org/elvecio)**<br/>~° Italian Translator<br/>
**[F&#42;&#42;&#42;OffGoogle](https://framagit.org/ajeremias)**<br/>~° Logo designer<br/>

## 0.8.9
* Edited Dandelion* references in contributors
* Edited Nomad channel references in share tag
* Edited F-Droid information text
## 0.8.8
* Changed App Logo
* Added visibility option for all menu items
## 0.8.7
* Changed App name to Nomad
## 0.0.6
* Added visibility option for icons in top bar
## 0.0.5
* Fixed search by tags and search by people after adding search by content
## 0.0.4
* Added search by content
## 0.0.3
* added Add-on setting in settings menu
* added suggestions in top menu
## 0.0.2
* moved some menu items in top menu
* started the visibility option as functionality but doesn't do what it supposed to do yet
## 0.0.1
* forked Nomad from Dandelion*
* changed all pods in Hubs   
* edited most of the things to fit for Hubzilla
* changed default colors and added them in color colorPicker
* changed some icons
* solved installation conflict with Dandelion*
